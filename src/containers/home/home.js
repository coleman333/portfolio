import React  from 'react';
import './style.css';

const HomePage = ()=> {

  return (
    <div className="homePageContainer" >
      <span className="homePageSpan homePageSpan1">Good day, </span>
      <span className="homePageSpan homePageSpan2">I`m Fullstack developer.</span>
      <span className="homePageSpan homePageSpan3">JavaScript Developer with 2 years experience, </span>
      <span className="homePageSpan homePageSpan4">worked in different projects from scratch </span>
      <span className="homePageSpan homePageSpan5">or maintaining the existing projects </span>
      <span className="homePageSpan homePageSpan6">This is my portfolio of real commercial projects.</span>
    </div>
  )
};

export default HomePage;
