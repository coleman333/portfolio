import wLingua1 from "./images/wLingua/wLingua1.png";
import wLingua2 from "./images/wLingua/wLingua2.png";
import wLingua3 from "./images/wLingua/wLingua3.png";
import wLingua4 from "./images/wLingua/wLingua4.png";
import wLingua5 from "./images/wLingua/wLingua5.png";
import wLingua6 from "./images/wLingua/wLingua6.png";
import wLingua7 from "./images/wLingua/wLingua7.png";
import wLingua8 from "./images/wLingua/wLingua8.png";
import drnk1 from "./images/DRNK/drnk1.png";
import drnk2 from "./images/DRNK/drnk2.png";
import drnk3 from "./images/DRNK/drnk3.png";
import drnk4 from "./images/DRNK/drnk4.png";
import drnk5 from "./images/DRNK/drnk5.png";
import inviteMe1 from "./images/inviteMe/inviteMe1.png";
import inviteMe2 from "./images/inviteMe/inviteMe2.png";
import inviteMe3 from "./images/inviteMe/inviteMe3.png";
import mixFit1 from "./images/mixFit/mixFit1.png";
import mixFit2 from "./images/mixFit/mixFit2.png";
import mixFit3 from "./images/mixFit/mixFit3.png";
import mixFit4 from "./images/mixFit/mixFit4.png";
import mixFit5 from "./images/mixFit/mixFit5.png";
import nda1 from "./images/nda/nda5.jpg";
import privetMir1 from "./images/privetMir/privetMir1.png";
import privetMir2 from "./images/privetMir/privetMir2.png";
import privetMir3 from "./images/privetMir/privetMir3.png";
import privetMir4 from "./images/privetMir/privetMir4.png";
import privetMir5 from "./images/privetMir/privetMir5.png";

export default {
  wLingua: [
    { image: wLingua1, title: 'w_lingua' },
    { image: wLingua2, title: 'w_lingua' },
    { image: wLingua3, title: 'w_lingua' },
    { image: wLingua4, title: 'w_lingua' },
    { image: wLingua5, title: 'w_lingua' },
    { image: wLingua6, title: 'w_lingua' },
    { image: wLingua7, title: 'w_lingua' },
    { image: wLingua8, title: 'w_lingua' },
  ],

  drnk: [
    { image: drnk1, title: 'drnk' },
    { image: drnk2, title: 'drnk' },
    { image: drnk3, title: 'drnk' },
    { image: drnk4, title: 'drnk' },
    { image: drnk5, title: 'drnk' },
  ],

  inviteMe: [
    { image: inviteMe1, title: 'invite_me' },
    { image: inviteMe2, title: 'invite_me' },
    { image: inviteMe3, title: 'invite_me' },
  ],

  mixFit: [
    { image: mixFit1, title: 'mix_fit' },
    { image: mixFit2, title: 'mix_fit' },
    { image: mixFit3, title: 'mix_fit' },
    { image: mixFit4, title: 'mix_fit' },
    { image: mixFit5, title: 'mix_fit' },
  ],

  nda: [
    { image: nda1, title: 'nda' }
  ],

  privetMir: [
    { image: privetMir1, title: 'privet_mir' },
    { image: privetMir2, title: 'privet_mir' },
    { image: privetMir3, title: 'privet_mir' },
    { image: privetMir4, title: 'privet_mir' },
    { image: privetMir5, title: 'privet_mir' },
  ],

}
